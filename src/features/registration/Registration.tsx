import React, { useState } from "react";
import { useForm } from "react-hook-form";
import styled from "@emotion/styled/macro";

interface RegistrationFormFields {
  username: string;
  password: string;
  confirmPassword: string;
}

const H1 = styled.h1`
  color: white;
  font-size: 25px;
  padding-bottom: 10px;
  border-bottom: 1px solid rgb(79, 98, 148);
`;

const Label = styled.label`
  line-height: 2;
  text-align: left;
  display: block;
  margin-bottom: 13px;
  margin-top: 20px;
  color: white;
  font-size: 14px;
  font-weight: 200;
`;

const Input = styled.input`
  display: block;
  box-sizing: border-box;
  width: 100%;
  border-radius: 4px;
  border: 1px solid white;
  padding: 10px 15px;
  margin-bottom: 10px;
  font-size: 14px;
`;

const Error = styled.p`
  color: #ff5c8d;
  font-size: 14px;
  font-weight: 200;
`;

const SubmitButton = styled.button`
  background: #ec5990;
  color: white;
  text-transform: uppercase;
  border: none;
  margin-top: 40px;
  padding: 20px;
  font-size: 16px;
  font-weight: 100;
  letter-spacing: 10px;
  display: block;
  appearance: none;
  border-radius: 4px;
  width: 100%;
`;

export function Registration() {
  const [state, setState] = useState("initial");
  const {
    register,
    handleSubmit,
    getValues,
    formState: { errors },
  } = useForm<RegistrationFormFields>({ mode: "onChange" });

  const validateConfirmPassword = () => {
    const password = getValues("password");
    const confirmPassword = getValues("confirmPassword");
    return password !== confirmPassword
      ? "Confirm password does not match with password"
      : undefined;
  };

  return (
    <div>
      <H1>Registration Form</H1>
      {state === "initial" && (
        <form
          onSubmit={handleSubmit((data) => {
            console.log(data);
            setState("done");
          })}
        >
          <Label htmlFor="username">User Name</Label>
          <Input
            {...register("username", {
              required: "Username is required",
              pattern: {
                value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                message: "Invalid Email Address",
              },
            })}
            id="username"
          ></Input>
          {errors.username && <Error>{errors.username.message}</Error>}
          <Label htmlFor="password">Password</Label>
          <Input
            type="password"
            {...register("password", {
              required: "Password is required",
              pattern: {
                value:
                  /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]/,
                message:
                  "Password must contain 1 number and 1 special character",
              },
            })}
            id="password"
          ></Input>
          {errors.password && <Error>{errors.password.message}</Error>}
          <Label htmlFor="confirmPassword">Confirm Password</Label>
          <Input
            type="password"
            {...register("confirmPassword", {
              validate: validateConfirmPassword,
            })}
            id="confirmPassword"
          ></Input>
          {errors.confirmPassword && (
            <Error>{errors.confirmPassword.message}</Error>
          )}
          <SubmitButton type="submit">Register</SubmitButton>
        </form>
      )}
      {state === "done" && <H1>Registration Complete</H1>}
    </div>
  );
}
