import { Registration } from "./Registration";

import { render, fireEvent, waitFor, screen } from "@testing-library/react";

describe("Registration Page", () => {
  test("Registration Complete", async () => {
    const registrationForm = render(<Registration />);
    fireEvent.change(registrationForm.getByLabelText("User Name"), {
      target: { value: "test@test.com" },
    });
    fireEvent.change(registrationForm.getByLabelText("Password"), {
      target: { value: "password@123" },
    });
    fireEvent.change(registrationForm.getByLabelText("Confirm Password"), {
      target: { value: "password@123" },
    });
    fireEvent.click(screen.getByText("Register"));
    await waitFor(() => screen.getByText("Registration Complete"));
  });

  test("Username is required", async () => {
    const registrationForm = render(<Registration />);
    fireEvent.click(screen.getByText("Register"));
    await waitFor(() => screen.getByText("Username is required"));
  });

  test("Username should be an email", async () => {
    const registrationForm = render(<Registration />);
    fireEvent.change(registrationForm.getByLabelText("User Name"), {
      target: { value: "test@test" },
    });
    fireEvent.click(screen.getByText("Register"));
    await waitFor(() => screen.getByText("Invalid Email Address"));
  });

  test("Password is required", async () => {
    const registrationForm = render(<Registration />);
    fireEvent.click(screen.getByText("Register"));
    await waitFor(() => screen.getByText("Password is required"));
  });

  test("Password should contain at least one number", async () => {
    const registrationForm = render(<Registration />);
    fireEvent.change(registrationForm.getByLabelText("Password"), {
      target: { value: "test@test" },
    });
    fireEvent.click(screen.getByText("Register"));
    await waitFor(() =>
      screen.getByText("Password must contain 1 number and 1 special character")
    );
  });

  test("Password should contain at least one special character", async () => {
    const registrationForm = render(<Registration />);
    fireEvent.change(registrationForm.getByLabelText("Password"), {
      target: { value: "testtest1" },
    });
    fireEvent.click(screen.getByText("Register"));
    await waitFor(() =>
      screen.getByText("Password must contain 1 number and 1 special character")
    );
  });

  test("Confirm Password should match password", async () => {
    const registrationForm = render(<Registration />);
    fireEvent.change(registrationForm.getByLabelText("Password"), {
      target: { value: "password@123" },
    });
    fireEvent.click(screen.getByText("Register"));
    await waitFor(() =>
      screen.getByText("Confirm password does not match with password")
    );
  });
});
