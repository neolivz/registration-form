import React from "react";
import styled from "@emotion/styled/macro";
import { Global, css } from "@emotion/react/macro";

import { Registration } from "./features/registration/Registration";

const AppContainer = styled.div``;

function App() {
  return (
    <div>
      <Global
        styles={css`
          body {
            background: #002f6c;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto",
              "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans",
              "Helvetica Neue", sans-serif;
            color: white;
            padding: 0 20px 100px;
          }
        `}
      />
      <Registration />
    </div>
  );
}

export default App;
